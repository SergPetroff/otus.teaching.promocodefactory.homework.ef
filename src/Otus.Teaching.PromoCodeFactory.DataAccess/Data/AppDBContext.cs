﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class AppDBContext: DbContext
    {
        
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {
           
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles{ get; set; }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlite("Data Source=Promocode.db;");
            //Подключаем логирование
            optionsBuilder.LogTo(System.Console.WriteLine, LogLevel.Information);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preference)
                .WithMany(s => s.Customer)
                .UsingEntity(j => j.ToTable("CustomerPreference"));
                
        }
        
    }
}
