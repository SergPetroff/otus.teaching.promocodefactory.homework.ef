﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.EFCore
{
    public class EFCoreRepository<TEntity> : IRepository<TEntity> 
        where TEntity : BaseEntity
        
    {

        protected AppDBContext _ctx;

        public EFCoreRepository(AppDBContext context)
        {
            _ctx = context;
        }
        public async Task<TEntity> AddByIdAsync(TEntity item)
        {
            await _ctx.Set<TEntity>().AddAsync(item);
            await _ctx.SaveChangesAsync();
            return item;
        }

        public async Task<TEntity> DeletByIdAsync(Guid id)
        {
           var finditem =  _ctx.Set<TEntity>().FirstOrDefault(item => item.Id == id);
            if(finditem != null)
            {
                 _ctx.Set<TEntity>().Remove(finditem);
                await _ctx.SaveChangesAsync();
            }

            return finditem;            
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _ctx.Set<TEntity>().ToListAsync();
        }

        public async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await _ctx.Set<TEntity>().FirstOrDefaultAsync(x => x.Id == id); //_ctx.Set<TEntity>().FindAsync(id);
        }

        public async Task<TEntity> UpdateByIdAsync(TEntity item)
        {
            _ctx.Entry(item).State = EntityState.Modified;
            await _ctx.SaveChangesAsync();
            return item;
        }
    }
}
