﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerReposytory<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetCustomersAsync();

        Task<T> GetCustomerAsync(Guid id);
        Task<T> DeleteCustomer(Guid id);
        Task<T> EditCustomersAsync(T entity);
        Task<T> CreateCustomerAsync(T entity);
    }
}
