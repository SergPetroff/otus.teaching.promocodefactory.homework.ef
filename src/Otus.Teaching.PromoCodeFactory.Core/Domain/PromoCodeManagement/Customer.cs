﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(50)]
        public string Email { get; set; }

        public List<Preference> Preference { get; set; } = new List<Preference>();

        public PromoCode PromoCode { get; set; }



        //TODO: Списки Preferences и Promocodes 
    }
}